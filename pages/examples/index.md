---
title: Example API Usage in Python
---

```python
import requests

auth_header = {'Authorization': 'my-api-key'}

field_data = {
  'geometry': {
    'type': 'Polygon',
    'coordinates': [[
       [-118.0079269, 45.4132890],
       [-118.0079269, 45.4168437],
       [-118.0015110, 45.4168437],
       [-118.0015110, 45.4132890],
       [-118.0079269, 45.4132890]
    ]]
  },
  'attributes': {
    'optional': 'key-value-attributes'
  }
}

response = requests.post('https://sat-api.upstream.tech/monitored-entities', data=field_data, headers=auth_header)
print(response.status_code) # => 200

entity = response.json()['entity']

# Entity will be processing for at least a minute.

response = requests.get('https://sat-api.upstream.tech/monitored-entities/' + entity['id'], headers=auth_header)
entity = response.json()['entity']
print(entity['status']) # => 'processing' or 'monitoring'

for imagery in entity['imagery']:
  print(imagery['date'])
  print(imagery['bounds'])
  print(imagery['ndvi_url'])
  print(imagery['moisture_url'])
```
