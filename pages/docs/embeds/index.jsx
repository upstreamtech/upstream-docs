import React from 'react';
import DocumentTitle from 'react-document-title';
import {config} from 'config';

const iframeStyle = {
  width: '100%',
  height: '560px',
  border: 0,
  backgroundColor: 'transparent',
  boxShadow: 'rgba(0, 0, 0, 0.2) 0 0 2px',
};
const iframeStyleString = 'width: 100%; height: 560px; border: 0; background-color: transparent; box-shadow: rgba(0,0,0,0.2) 0 0 2px;';
const iframeString = `<iframe datasrc="https://upstream.tech/embed/32a72ef0-320f-495a-b695-7b5c6f0bb53c?sat=L8" style="${iframeStyleString}"/>`;

const Embeds = React.createClass({
  render () {
    return (
      <DocumentTitle title={`Embeds | ${config.siteTitle}`}>
        <div>
          <h1>Embeds</h1>
          <p>
            It's easy to include satellite imagery timelines with multiple data
            layers in your product. By embedding Upstream's Monitor Card, you
            can save yourself the time integrating map libraries and displaying
            data.
          </p>
          <p>
            By including the following snippet on your website, you can
            immediately have auto-updating satellite imagery of your monitored
            entities, with calculated layers such as NDVI and Upstream Soil
            Moisture.
          </p>
          <pre>
            {iframeString}
          </pre>
        </div>
      </DocumentTitle>
    )
  },
});


export default Embeds;
exports.data = {
  title: 'Embeds',
};
