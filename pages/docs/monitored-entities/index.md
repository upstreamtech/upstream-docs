---
title: Monitored Entities
---

A monitored entity represents an area on the ground for which the Sat API maintains up to date satellite imagery.

### Register A Monitored Entity
  ```
  POST https://sat-api.upstream.tech/monitored_entities

  {
    "geometry": {
      "type": "Polygon",
      "coordinates": [
        [[100.0, 0.0], [101.0, 0.0], [101.0, 1.0], [100.0, 1.0], [100.0, 0.0]]
      ]
    },
    "attributes": {
      "your_id": 12345,
      "custom_field": "custom value"
    }
  }
  ```
  The `geometry` value must be a valid geojson object of type `Polygon`, see http://geojson.org/geojson-spec.html#id4 for examples. Note: the geojson specification requires each point coordinate to be specified with the longitude first `[longitude, latitude]`. The First and last points of each line in the polygon must also be the same.

  The `attributes` object is optional and allows you to attach metatdata for your application to the monitored entity.

  Example response
  ```
  {
    "entity": {
      "id": "68d884cf-d4e8-47dd-a198-dafa812e1bc9",
      "status": "processing",
      "geometry": <geojson geometry>,
      "attributes": {
        "your_id": 12345,
        "custom_field": "custom value"
      },
      "imagery": []
    }
  }
  ```

  After the entity is created, we will begin processing imagery for that entity. The `status` of the entity will remain `processing` until all existing data as been processed, at which point it will become `monitoring`.

### Fetch Monitored Entity Data
```
GET https://sat-api.upstream.tech/monitored_entities/68d884cf-d4e8-47dd-a198-dafa812e1bc9
{
  "entity:" {
    "id": "68d884cf-d4e8-47dd-a198-dafa812e1bc9",
    "status": "monitoring",
    "geometry": <geojson geometry>,
    "attributes": {
      "your_id": 12345,
      "custom_field": "custom value"
    },
    "imagery": [
      {
         "date": "2016-09-03T16:45:23.36900Z",
         "bounds": [
           [45.335, -118.022],
           [45.393, -117.935]
         ],
         "moisture_url": "https://storage.googleapis.com/upstream-images/xxx.png",
         "ndvi_url": "https://storage.googleapis.com/upstream-images/xxx.png"
      },
      {
         "date": "2016-09-13T16:45:23.36900Z",
         "bounds": [
           [45.335, -118.022],
           [45.393, -117.935]
         ],
         "moisture_url": "https://storage.googleapis.com/upstream-images/xxx.png",
         "ndvi_url": "https://storage.googleapis.com/upstream-images/xxx.png"
      }
    ]
  }
}
```
The `imagery` objects are be sorted by date, ascending. The bounds for each each object are of the form required by the `imageOverlay` of Leaflet.js, see http://leafletjs.com/reference.html#imageoverlay.

The `imagery` array contains all available, relatively cloud free imagery for the for 6 months leading up to the present. Contact team@upstream.tech for imagery covering a longer time period or highlighting additional land cover types.

### List Monitored Entities

Listing all monitored entities returns a similar response to fetching a single entity, however the each entity will not have an `imagery` key.

```
GET https://sat-api.upstream.tech/monitored_entities?offset=0&limit=10
{
  "entities": [
    {
      "id": "68d884cf-d4e8-47dd-a198-dafa812e1bc9",
      "status": "monitoring",
      "geometry": <geojson geometry>,
      "attributes": {
        "your_id": 12345,
        "custom_field": "custom value"
      }
    }
    {
      "id": "a3eb2c8e-723c-436d-8076-cc38f45d3c7b",
      "status": "monitoring",
      "geometry": <geojson geometry>,
      "attributes": {}
    }
  ]
}
```
The `offset` and `limit` query parameters are optional and default to 0 and 10 respectively.
