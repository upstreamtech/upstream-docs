---
title: "Upstream SatAPI"
---

**Upstream SatAPI** powers remote monitoring of water-centric entities. Currently our API allows:

Creating monitored entities
- Register any GeoJSON shape to be monitored. We will extract recent historical satellite imagery as visual png images of the entity and its surrounding area representing vegetation health (NDVI) and Upstream Soil Moisture. Every time one of our satellites passes the monitored entity, we create new products and imagery automatically.
- Retrieve images. At any time you can make a web request to access all historical imagery of a monitored entity.
- Retrieve data products. We currently provide NDVI, moisture, and various statistical products. More coming soon.

**Contact `team@upstream.tech` to get API access credentials.**

### Data Sources
We currently provide data from two satellites - NASA/USGS's Landsat 8 and the European Space Agency's Sentinel-2A.

**Spatial Resolution** describes the area on the ground covered by a single pixel of satellite data. The data we access from Sentinel-2A has 10 meter spatial resolution, meaning a pixel covers a 10m by 10m square of ground. Landsat 8 has 30m spatial resolution for our NDVI data, while the moisture data is 100m rescaled to match the 30m data.

**Temporal Resolution** describes how often an orbiting satellite images the same location on earth, also known as it's revisit time. Sentinel-2A has a 10 day revisit time, while Landsat 8 has a 16 day revisit time. If the monitored area is too covered in clouds on a particular day we will not provide imagery for for that day. By combining access to data from both of these satellites we are able to provide a higher effective revisit time than working with a single satellite.

### REST API
The Upstream Satellite API is a JSON based REST API. All requests are made over HTTPS and must be authenticated.

### Authentication
All API requests are authenticated with the `Authorization:` HTTP header with your API key as the value.
```
curl -H "Authorization: <API key>" https://sat-api.upstream.tech/monitored_entities
```
The Authorization header is ommitted from examples for brevity, but is always required.
